<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailToLegislators extends Mailable
{
    use Queueable, SerializesModels;

    public $sender;
    public $letter;
    public $recipient;
    public $headers;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $letter, $recipient, $emailId)
    {
        $this->headers = ['X-Mailgun-Variables' => '{"email_id": "'. $emailId .'"}'];
        $this->sender = $sender;
        $this->letter = $letter;
        $this->recipient = $recipient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        sleep(2);

        return $this
        ->from($this->sender['email'], $this->sender['name'])
        ->subject(env('EMAIL_SUBJECT_LINE'))
        ->view(['emails.legislator-html', 'emails.legislator-text']);
    }
}
