<?php

namespace App\Http\Controllers;

use App\Mail\EmailToLegislators;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Log;

class EmailController extends Controller
{
    public function post(Request $request){

        $emails_sent = 0;
        $letter = $request->letter;
        $recipients = $request->recipients;
        $sender['name'] = $request->sender_name;
        $sender['email'] = $request->sender_email;
        $sender['zipcode'] = $request->zipcode;

        $sender = $this->formatSenderData($sender);
        $recipients = $this->formatRecipients($recipients);

        //If Zipcode isn't valid, send user back with error.
        if ($sender['zipcode'] == '0')
        {
            return redirect()->back()->with('status', 'We need your City and Zipcode to send your messages.');
        }

        //Check if it's a valid WV Zipcode
        if( $sender['zipcode'] < 24600 || $sender['zipcode'] > 27000 )
        {
            return redirect()->back()->with('status', 'This site is dedicated to West Virginia Residents Only.<br />If your state is fighting a new tax, contact us for an app!<br />info@hackneyenterprises.com');
        }

        foreach ($recipients as $recipient){
            $recipient = json_decode(json_encode($recipient), FALSE);

            $emailId = DB::table('emails')->insertGetId([
                'recipient_email' => $recipient->email,
                'recipient_name' => $recipient->name,
                'sender_email' => $sender['email'],
                'sender_name' => $sender['name'],
                'sender_zipcode' => $sender['zipcode'],
                'sent_at' => Carbon::now()
            ]);

            Mail::to($recipient)->queue(new EmailToLegislators($sender, $letter, $recipient, $emailId));

            $emails_sent++;


        }

        return redirect('/')->with('status', $emails_sent . ' email(s) sent!');
    }

    public function handleWebhook(Request $request){
        $data = $request->all();

        $eventData = $data['event-data'];
        $senderEmail = $eventData['envelope']['sender'];
        $recipientEmail = $eventData['recipient'];
        $timeStamp = Carbon::createFromTimestamp($eventData['timestamp']);

        switch (strtolower($eventData['event'])){
            case 'delivered':
                $email = DB::table('emails')->where('sender_email', $senderEmail)->where('recipient_email', $recipientEmail)->first();
                if($email){
                    $updated = DB::table('emails')->whereId($email->id)->update([
                        'delivered_at' => $timeStamp,
                        'message_id' => $eventData['id']
                    ]);

                    if($updated) Log::info("[WEB HOOK RECEIVED] Updated Delivered At For Email ID {$email->id}");
                    if(!$updated) Log::error("[WEB HOOK RECEIVED] Could Not Update Delivered At For Email ID {$email->id}");

                }
                break;
            case 'opened':
                $email = DB::table('emails')->where('sender_email', $senderEmail)->where('recipient_email', $recipientEmail)->where('delivered_at', '!=', null)->first();
                $updated = DB::table('opens')->whereId($email->id)->update([
                    'email_id' => $email->id,
                    'opened_at' => $timeStamp,
                    'recipient_location' => $eventData['geolocation']['city'] . ', ' . $eventData['geolocation']['state'] . ' ' . $eventData['geolocation']["country"],
                    'recipient_city' => $eventData['geolocation']['city'],
                    'recipient_state' => $eventData['geolocation']['state'],
                    'recipient_country' => $eventData['geolocation']["country"],
                    'ip_address' => $eventData['ip'],
                    'device_type' => $eventData['client-info']['device_type'],
                    'client_name' => $eventData['client-info']['client_name'],
                    'client_type' => $eventData['client-info']['client_type'],
                    'user_agent' => $eventData['client-info']['user_agent'],
                    'message_id' => $eventData['id'],
                ]);

                if($updated) Log::info("[WEB HOOK RECEIVED] Updated Opened At For Email ID {$email->id}");
                if(!$updated) Log::error("[WEB HOOK RECEIVED] Could Not Update Opened At For Email ID {$email->id}");
                break;
            case 'complained':
                DB::table('complaints')->insert([
                    'email' => $recipientEmail,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
                Log::info("[WEB HOOK] Received Webhook Complaint Logged");
            default:
                Log::info("[WEB HOOK] Received Webhook and didnt have a case for it.");
                Log::info("[WEB HOOK] Data: " . json_encode($data));
        }
    }

    public function handleReply(Request $request){
        $data = $request->all();
        DB::table('replies')->insert([
            'recipient_email' => $data['recipient'],
            'sender_email' => $data['sender'],
            'subject' => $data['subject'],
            'body' => $data['body-plain'],
            'received_at' => Carbon::now()
        ]);

        return response('Got It', 200);
    }


    public function formatSenderData($sender){
        //Format Zipcode to be an interval
        $sender['zipcode'] = intval(str_replace('+', ' ', filter_var($sender['zipcode'], FILTER_SANITIZE_NUMBER_INT)));

        //Senders Name isn't present or is just one word, set a default
        if ($sender['name'] == '' || str_word_count($sender['name']) < 2){
            $sender['name'] = 'Taxpayer from ' . $sender['zipcode'];
        }
        //Senders Email isn't present, set a default
        if ($sender['email'] == ''){
            $sender['email'] = env('MAIL_FROM_ADDRESS');
        }

        return $sender;

    }

    public function formatRecipients($recipients)
    {

        $recipients = explode( ',', $recipients );

        array_push($recipients, 'governor@wv.gov', 'Governor Justice');

        $recipients = array_chunk($recipients, 2);

        if(env('TESTING')){
            unset($recipients);
            $recipients = [];
            array_push($recipients, env('TESTING_EMAIL'), 'TEST MODE USER');
            $recipients = array_chunk($recipients, 2);
        }

        for ($i=0; $i < count($recipients); $i++)
        {
            $recipients[$i]['email'] = $recipients[$i][0];
            $recipients[$i]['name'] = $recipients[$i][1];
            unset($recipients[$i][0]);
            unset($recipients[$i][1]);
        }

        return $recipients;

    }

    public function getEmailNameAndAddress($data) {

        $data = explode(' <', $data);
        $formatted_data = [];
        if (is_array($formatted_data)) {
            $formatted_data['name'] = $data[0];
            $formatted_data['email'] = str_replace('>', '', $data[1]);
        }
        else {
            $formatted_data['name'] = '';
            $formatted_data['email'] = $data;
        }

        return $formatted_data;
    }

    public function trimBrackets($string){

        $string = trim($string, '<>');

        return $string;

    }

}
