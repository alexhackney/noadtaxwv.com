<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;
use Mail;

class SendHourlyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:hourly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a report on the last hours emails. If there were more than 30.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = DB::table('emails')
                    ->where('sent_at', '>', Carbon::now()->subHours(1))
                    ->get();


        $opens = DB::table('opens')
                    ->where('opened_at', '>', Carbon::now()->subHours(1))
                    ->get();

        $opens = $opens->unique('recipient_email');

        if($emails->count() > 30){
            Mail::send('emails.hourly-report', ['emails' => $emails, 'opens' => $opens], function ($m) use ($emails, $opens) {
                $m->to(env('REPORTS_ADDRESS'), env('REPORTS_NAME'))->subject('NoAdTaxWV Hourly Report for ' . Carbon::now()->toDateTimeString());
            });
        }
        
        return 'No Emails Sent';

    }
}
