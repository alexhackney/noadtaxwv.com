<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;
use Mail;

class SendDailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily report of email totals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = DB::table('emails')->where('sent_at', '>', Carbon::createFromDate('2021', '3', '1'))->get();
        $yesterday_emails = $emails
                            ->where('sent_at', '>', Carbon::today()->subDays(1))
                            ->where('sent_at', '<', Carbon::today())->count();


        $opens = DB::table('opens')->get();
        /*
        $total_opens = count($opens);
        $yesterday_opens = $opens
                            ->where('opened_at', '>', Carbon::today()->subDays(1))
                            ->where('opened_at', '<', Carbon::today());
        */
        $to = explode(',', env('REPORT_ADDRESSES'));
        if($yesterday_emails > 1){
            foreach($to as $to) {
                Mail::send('emails.daily-report', ['emails' => $emails, 'yesterday_emails' => $yesterday_emails], function ($m) use ($emails, $yesterday_emails, $to) {
                    $m->to($to)
                        ->subject('NoNewTaxesWV.com Daily Report for ' . Carbon::yesterday()->toFormattedDateString());
                });
            }
        }
    }
}
