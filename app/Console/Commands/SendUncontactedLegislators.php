<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use DB;
use Mail;
use Carbon\Carbon;

class SendUncontactedLegislators extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:uncontacted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a list of uncontacted legislators';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $legislators = null;

        $client = new Client();
        $data = $client->request('GET', 'https://openstates.org/api/v1/legislators/?state=wv&active=true');

        $all_legislators = collect(json_decode($data->getBody()));

        $all_emails = $all_legislators->pluck('offices.0.email');

        $contacted_emails = DB::table('emails')
        ->orderBy('recipient_email')
        ->pluck('recipient_email')
        ->unique();

        
        $contacted_emails;

        $uncontacted_emails = $all_emails->diff($contacted_emails);

        $uncontacted_legislators = $all_legislators
        ->whereNotIn('offices.0.email', $contacted_emails)->sortBy('last_name');



        Mail::send('emails.uncontacted_legislators', ['all_legislators' => $all_legislators, 'uncontacted_legislators' => $uncontacted_legislators], function ($m) use ($all_legislators, $uncontacted_legislators) {
            $m->to(env('REPORTS_ADDRESS'), env('REPORTS_NAME'))->subject('NoAdTaxWV Uncontacted Legislators Report for ' . Carbon::now()->toDateTimeString());
            });



        
    }
}
