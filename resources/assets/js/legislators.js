$( ".legislators-form" ).submit(function(event) {
	//Prevent Form Submission
	event.preventDefault();

	//Format the address
	var address = $( "#address" ).val().replace(/ /g, '+').replace(',', '');

	//console.log(address);

	//Hide the collection form and show the email section
	$('.legislators-form').hide();
	$('.legislators-email').delay( 300 ).fadeIn( 300 );

	var emails = [];

	//AJAX request to get the address' lat + long
	$.ajax({
		url:"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD5SlYN47-2WtKKSEq-GKdwHDLd_wPjCyg&address=" + address,

		dataType: "json",
		success: function(response){
		    //console.log(response);

			lat = response.results[0].geometry.location.lat;
			lng = response.results[0].geometry.location.lng;
			//console.log('Got Lat: ' + lat + ' and lng: ' + lng);

			//AJAX request to get the legislators with the lat and long
			$.ajax({
				url: "https://v3.openstates.org/people.geo?apikey=f21898ef-e67a-4abb-9c3d-d7cbd57c85d9&lat=" + lat + "&lng=" + lng,
				cache:false,
				//dataType: "application/json",
				success: function(data) {

                    let legislators = data.results;
                    $.each(legislators, function(item) {
                        if(legislators[item].name == 'Corey Palumbo'){
                            legislators[item].name = 'Eric Nelson';
                            legislators[item].email = 'eric.nelson@wvsenate.gov';
                            legislators[item].image = '/images/eric-nelson.jpg';
                        }
                        if(legislators[item].name == 'Roman Prezioso'){
                            legislators[item].name = 'Mike Caputo';
                            legislators[item].email = 'mike.caputo@wvsenate.gov';
                            legislators[item].image = '/images/mike-caputo.jpg';
                        }
                    });
					//Append the legislator data to the view.
					$.each(legislators, function(item) {
			           	$( '.legislators').append('<div class="col-xs-6 legislator"> <img class="img img-responsive" src="' +
                            legislators[item].image.replace(/^http?/i, "https").replace("www.legis.state.wv.us", "www.wvlegislature.gov") + '"><h4>' + legislators[item].name +
                            '</h4><p><a href="mailto:' + legislators[item].email +
                            '?subject=RE: The New West Virginia Income Tax">' + legislators[item].email +
                            '</a></p>');

			           	emails.push(legislators[item].email,legislators[item].name);

			       	});
			       	$('form').append('<input type="hidden" name="zipcode" value="' + address + '">');
			       	$('form').append('<input type="hidden" name="recipients" value="' + emails + '">');
				}
			})
		}
	});
	//Reinit Tinymce due to not showing on older browsers.
	tinymce.init({selector:'textarea'});
});

