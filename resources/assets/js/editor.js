tinymce.init({
	selector: 'textarea',
	height: 450,
	menubar: false,
	plugins: [
		'autolink link print preview'
//		'searchreplace fullscreen',
//		'insertdatetime media contextmenu paste'
	],
	toolbar: 'undo redo | link | bold italic underline | preview print'
});