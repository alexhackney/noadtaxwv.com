@extends('master')

@section('content')
<div class="container">
	@include('partials.confirmation')
	<div class="row">
		<div class="col-md-6 col-md-offset-1">
			<h2 class="text-center">HERE’S WHY:</h2>
				<ul>
                    <li>State Leaders have proposed a bill to raise <span style="font-weight:bold;">over a half billion</span> in new taxes.</li>
                    <li>This is the largest tax increase in the history of West Virginia!</li>
                    <li>Sales Taxes will be increased as much as 31-percent.</li>
                    <li>WV will have the highest consumer sales tax in the nation at the rate of 7.9% (then add the 1% municipal tax) Higher than California, New York and surrounding states.</li>
                    <li>This increase could not happen at a worse time, businesses and families are struggling coming out of this devastating pandemic.</li>
                    <li>The taxes proposed will send an anti-business signal and give unfair advantage to out-of-state competitors.</li>
                    <li>A tax on advertising hinders public access to news and information. 40 states have considered and rejected the idea.</li>
                    <li>Businesses on the boarder will experience huge losses with this plan.</li>
                    <li>Business and families will be impacted by more than just one or two of these taxes. The combination will greatly add to their bottom line.</li>
                    <li>The proposal still leaves a funding gap of $152 million dollars.</li>
                    <li>Businesses will think twice before coming to WV or staying here with this tax hike.</li>

				</ul>
		</div>
		<div class="col-md-4">
				@include('partials.learn-more')
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<hr class="red">
		</div>
	</div>


    @if(env('.e'))
	    @include('partials.contact-your-legislators')
        @include('partials.hr-red')
        @include('partials.social')
    @else
        @include('partials.not-enabled')
    @endif

	<p class="text-center small">We protect your privacy</p>

</div>


@endsection
