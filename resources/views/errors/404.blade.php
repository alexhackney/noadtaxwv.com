<!DOCTYPE html>
<html>
<head>
	<title>Page not found!</title>
	<style>
	body {
			background-color: black;
			font-family: arial;
			color:white;
			text-align: center;
		}
		h1 {
			color:red;
			margin-top:3em;
		}
		a, a:visited {
			color:white;
			text-decoration: none;
		}
		a:hover {
			color:orange;
		}
	</style>
</head>
<body>
<h1>Page Not Found</h1>
<p><a href="/">Please click here to return to the main page</a></p>

@include('partials.google-analytics')
</body>
</html>