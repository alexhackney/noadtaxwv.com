<html>
<p>Automated No New Taxes WV Report</p>
<p>Emails Sent Yesterday: {{ $yesterday_emails }}</p>
<p>Emails Sent Total: {{ $emails->count() }}</p>
<p>Emails have been sent to {{ $emails->unique('recipient_email')->count() }} different legislators to date.</p>
<p>For more app details check <a href="http://google.com/analytics">Google Analytics.</a></p>
</html>
