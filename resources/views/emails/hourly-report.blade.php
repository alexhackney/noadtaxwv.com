<html>
<p>Hello</p>
<p>Stats for the last hour.</p>
<p>Emails Sent: {{ $emails->count() }}</p>
<p>Emails Opened Last Hour: {{ $opens->count() }}</p>
<p>Emails Opened By The Following People:</p>
<p>
	<ul>
		@foreach($opens as $open)
		 <li>{{ $open->recipient_name }}</li>
		@endforeach
	</ul>
</p>
<p>For more app details check <a href="http://google.com/analytics">Google Analytics.</a></p>
</html>