<html>
<p>Dear {{ $recipient->name }},</p>

{!! $letter !!}

<p>Sincerely,<br/>
{{ $sender['name'] }}<br/>
@if($sender['name'] == 'Taxpayer from ' . $sender['zipcode'])
</p>
@else
Taxpayer from {{ $sender['zipcode'] }}</p>
@endif

</html>