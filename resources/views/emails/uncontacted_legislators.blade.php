<p>Hello,</p>
<p>So far we have not contacted {{ $uncontacted_legislators->count() }} of {{ $all_legislators->count() }} total Legislators.</p>
<p>The uncontacted legislators are:<br/>
<ul>
@foreach($uncontacted_legislators as $legislator)
	<li>{{ $legislator->last_name }}, {{ $legislator->first_name }}</li>
@endforeach
</ul>
</p>
<p>Thank you!</p>