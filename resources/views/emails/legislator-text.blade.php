Dear {{ $recipient->name }},

{!!  strip_tags($letter) !!}

Sincerely,
{{ strip_tags($sender['name']) }}
@if($sender['name'] != 'Taxpayer from ' . $sender['zipcode'])
Taxpayer from {{ strip_tags($sender['zipcode']) }}
@endif
