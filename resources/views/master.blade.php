<!DOCTYPE html>
<html lang="en">
  <head itemscope itemtype="http://schema.org/WebSite">
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="description" content="The West Virignia Governor has suggested adding a new tax to the state. Here's why that's a bad idea.">
    	<meta name="author" content="WV Broadcasters Association">
    	<title>Say no to WV's New Tax Proposal!</title>
		<link rel="canonical" href="https://nonewtaxwv.com/">
		<!-- Latest compiled and minified CSS -->
		<!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->
		<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
		<link rel="icon" href="/favicon.png">
		<link rel="stylesheet" href="/css/style.css">

		@include('partials.opengraph')

	</head>
	<body>

		@include('partials.header')

		@yield('content')

		@include('partials.footer')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
		<script src="/js/app.js"></script>

		@yield('scripts')

		@include('partials.google-analytics')
	</body>

</html>
