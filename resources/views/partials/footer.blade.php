<footer>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 text-center">
                <p>
                    <a href="https://www.wvba.com/" rel="nofollow" target="_blank">
                        <img src="/images/wv-broadcasters-association.png" alt="Powered by the West Virginia Broadcasters Association">
                    </a>
                </p>
				<p>West Virginia Broadcasters Association • PO Box 8499 • So Charleston, WV 25303</p>
			</div>

			<div class="col-md-12 text-center" style="font-size:.75em;">
				<p>Trouble with this site? <a href="mailto:issues@nonewtaxeswv.com">issues@nonewtaxeswv.com</a></p>
			</div>
		</div>
	</div>
</footer>
