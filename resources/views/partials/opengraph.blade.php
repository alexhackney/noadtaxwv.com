<meta property="og:updated_time" content="{{\Carbon\Carbon::createFromTimestamp('1615838644')->toIso8601String()}}">
<meta property="og:site_name" content="THE PERSONAL INCOME TAX BILL: THE WRONG CHOICE FOR WV LAWMAKERS" />
<meta property="og:title" content="THE PERSONAL INCOME TAX BILL: THE WRONG CHOICE FOR WV LAWMAKERS" />
<meta property="og:type" content="website" />
<meta property="og:description" content="State Leaders have proposed a bill to raise over a half billion in new taxes. This is the largest tax increase in the history of West Virginia!"/>
<meta property="og:url" content="https://nonewtaxeswv.com" />
<meta property="og:image" content="https://nonewtaxeswv.com/images/no-new-taxes-wv-1200x627.jpg" />
<meta property="og:image:alt" content="State Leaders have proposed a bill to raise over a half billion in new taxes. This is the largest tax increase in the history of West Virginia!" />
<meta property="fb:app_id" content="899181630860396">

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="State Leaders have proposed a bill to raise over a half billion in new taxes. This is the largest tax increase in the history of West Virginia!" />
<meta name="twitter:image" content="https://nonewtaxeswv.com/images/no-new-taxes-wv-twitter-600x600.jpg" />
<meta name="twitter:image:alt" content="State Leaders have proposed a bill to raise over a half billion in new taxes. This is the largest tax increase in the history of West Virginia!" />
<meta name="twitter:url" content="https://nonewtaxeswv.com" />

