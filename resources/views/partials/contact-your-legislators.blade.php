<div class="row">
	<div class="col-md-12 text-center">
        <h2>CONTACT YOUR LEGISLATOR AND TELL THEM<br/>
            NO MORE ADDITIONAL TAXES ON WEST VIRGINIANS</h2>
        <p style="color:red;">The Personal Income Tax bill would be devastating to the<br/>
            business community small/large and consumers in West Virginia.</p>
        <p style="color:red;">Make sure your lawmakers in Charleston know the impact of their decisions.</p>
	</div>
</div>
<div class="row legislators-form">
	<div class="col-md-8 col-md-offset-2 text-center">
		<h3>FIND YOUR LOCAL LEGISLATORS TO SEND EMAIL NOW</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-4 col-md-offset-4 text-center">
		<form class="legislators-form">
			<div class="form-group">
				<label for="address">Enter your city and zipcode</label>
				<input type="text" id="address" class="form-control" pattern=".{5,}" title="We Need Your City and Zipcode to proceed." placeholder="ie. Charleston 25317" required>
			</div>
			<button id="button" class="btn btn-block">Display your Representatives</button>
		</form>
	</div>
</div>
<div class="row legislators-email">
	<div class="col-md-8 col-md-offset-2 text-center">
		<h3>Use this form to send your legislators a message</h3>
	</div>
</div>
<div class="row legislators-email">
	<div class="col-md-6">
		<div class="row">
			<div class="col-xs-6 governor">
				<img class="img img-responsive" src="/images/jim-justice.jpg" alt="West Virginia Governor Jim Justice">
				<h4>Jim Justice</h4>
				<p><a href="mailto:governor@wv.gov?subject=RE:%20West%20Virginia%20Ad%20Tax">governor@wv.gov</a></p>
				<!--<p><a href="callto:13045582000">304-558-2000</a></p>-->
			</div>
			<div class="legislators"></div>
		</div>
	</div>
	<div class="col-md-6">
		<h4>Dear Legislator,</h4>
		<form action="/send" method="POST">
			{{ csrf_field() }}
			<div class="row">
				<textarea class="form-control form-letter form-group" name="letter" rows="18">
@include('partials.form-letter')
				</textarea>
			</div>
			<div class="row">
				<div class="col-xs-3 col-xs-offset-3">
					<h4>Respectfully,</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-8 col-xs-offset-4">
					<div class="form-group">
	      				<input type="text" class="form-control" id="sender-name" name="sender_name" placeholder="Mr. or Ms. WV Tax Payer">
	      				<input type="email" class="form-control" id="sender-email" name="sender_email" placeholder="taxpayer@example.com">
	      				<p class="small text-center">Fields are Optional</p>
			  		</div>
		  		</div>
		  	</div>
	  		<div class="row">
	  			<div class="col-sm-6 col-sm-offset-3">
	  				<button type="submit" class="btn btn-block margin-top">Send your letter!</button>
	  			</div>
	  		</div>
		</form>
	</div>
</div>
