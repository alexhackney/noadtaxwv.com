	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2 margin-top">
		@if (session('status'))
    		<div class="alert alert-success text-center">
        		{!! session('status') !!}
    		</div>
		@endif
		</div>
	</div>
