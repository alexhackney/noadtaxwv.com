<?php

return [
    'facebook' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-facebook-square"></span></a></li>',
    'twitter' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-twitter"></span></a></li>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-linkedin"></span></a></li>',
    'whatsapp' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-whatsapp"></span></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-pinterest"></span></a></li>',
    'reddit' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-reddit"></span></a></li>',
    'telegram' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><span class="fa fa-2x fa-telegram"></span></a></li>',
];
