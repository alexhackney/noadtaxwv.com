<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('recipient_name');
            $table->string('recipient_email');
            $table->string('sender_name');
            $table->string('sender_email');
            $table->integer('sender_zipcode');
            $table->timestamp('sent_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emails');
    }
}