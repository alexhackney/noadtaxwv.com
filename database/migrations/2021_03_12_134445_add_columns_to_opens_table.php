<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOpensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opens', function (Blueprint $table) {
            $table->unsignedInteger('email_id')->nullable()->after('id');
            $table->string('recipient_location')->nullable()->change();
            $table->string('recipient_city')->nullable()->after('recipient_location');
            $table->string('recipient_state')->nullable()->after('recipient_city');
            $table->string('recipient_country')->nullable()->after('recipient_state');
            $table->ipAddress('ip_address')->nullable()->after('recipient_country');
            $table->string('device_type')->nullable()->after('ip_address');
            $table->string('client_name')->nullable()->after('device_type');
            $table->string('client_type')->nullable()->after('client_name');
            $table->string('user_agent')->nullable()->after('client_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opens', function (Blueprint $table) {
            $table->dropColumn([
                'email_id',
                'recipient_city',
                'recipient_state',
                'recipient_country',
                'ip_address',
                'device_type',
                'client_name',
                'client_type',
                'user_agent'
                ]);
        });
    }
}
