const { mix } = require('laravel-mix');

mix.combine([
	'resources/assets/css/bootstrap.css',
	'resources/assets/css/fonts.css',
	'resources/assets/css/font-awesome.css',
	'resources/assets/css/style.css',
	'resources/assets/css/tinymce/skin.min.css',
	'resources/assets/css/tinymce/skin.ie7.min.css',
	], 'public/css/style.css')
	.js([
	'resources/assets/js/jquery-3.1.1.min.js',
	'resources/assets/js/bootstrap.js',
	'resources/assets/js/tinymce.js',
	'resources/assets/js/tinymce/autolink/plugin.js',
	'resources/assets/js/tinymce/fullscreen/plugin.js',
	'resources/assets/js/tinymce/link/plugin.js',
	'resources/assets/js/tinymce/preview/plugin.js',
	'resources/assets/js/tinymce/print/plugin.js',
	'resources/assets/js/tinymce/themes/modern/theme.js',
	'resources/assets/js/editor.js',
	'resources/assets/js/legislators.js',
	], 'public/js/app.js');
